package exception;

/**
 * 
 * @author Helena Zaccarelli
 * 
 *    Evita che vengano effettuate operazioni non possibili/previste
 */

public class ExceptionNotPossibleOp extends Throwable {
	public static final long serialVersionUID = 1L;
}



